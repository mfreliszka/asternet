package Main;
import java.util.Map;
public class ReadDataThread implements Runnable{
    ConnectionManager manager;
    volatile boolean stop;
    Thread th;

    public ReadDataThread(ConnectionManager manager){
        this.manager = manager;
        this.stop = false;
        th=new Thread(this);
    }

    public void run() {

        while (!stop) {
            if (manager.Client.Connected) { // status: connected

                for (Map.Entry<Integer,Silnik> para: Main.silniki.entrySet()) {
                    System.out.format("czytam silnik %d \n", para.getKey());
                    para.getValue().czytajDane();
                }

                for (Map.Entry<Integer,Poziom> para: Main.poziomy.entrySet()) {
                    System.out.format("czytam poziom %d \n", para.getKey());
                    para.getValue().czytajDane();
                }
            } else {

            }
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                System.out.println("Exception thrown!");
            }
        }
    }

    synchronized public void start(){
        stop = false;
        th.start();
    }

    synchronized public void stop() throws InterruptedException{
        stop = true;
        th.join();
    }
}
