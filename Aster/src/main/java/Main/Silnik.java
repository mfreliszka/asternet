package Main;

import com.sourceforge.snap7.moka7.S7;
import javafx.animation.RotateTransition;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import javafx.scene.shape.Rectangle;

//import javax.xml.bind.annotation.*;

//@XmlRootElement(name = "silnik")
//@XmlAccessorType(XmlAccessType.FIELD)
public class Silnik {
    public boolean[] dataInput = new boolean[16];
    /*
    0 - start_in
    1 - auto_in
    */
    public boolean[] data = new boolean[16];
    /*
    0 - praca
    1 - awaria
    2 - auto
    3 - startAuto
     */
    public String name;
    public int addressInDB;
    public int DBnum;
    public int addressInDB_in;
    public int DBnum_in;
    ConnectionManager manager;
    Rectangle status;
    public static byte[] Buffer = new byte[2];
    public static byte[] Buffer_in = new byte[2];

    public Silnik(String name, int DBnum, int addressInDB, int DBnum_in, int addressInDB_in, ConnectionManager manager,  Rectangle status){
        this.name = name;
        this.DBnum = DBnum;
        this.addressInDB = addressInDB;
        this.DBnum_in = DBnum_in;
        this.addressInDB_in = addressInDB_in;
        this.manager = manager;
        this.status = status;
        System.out.println(this.name + " created!");
    }

    public void czytajDane(){
        if(!manager.Client.Connected) {
            return;
        }
        manager.Client.ReadArea(S7.S7AreaDB, DBnum, addressInDB, 2, Buffer);
        data = manager.toBoolArray(S7.GetWordAt(Buffer, 0));

        if (data[8]){ //praca
            status.setFill(Color.GREEN);
            RotateTransition rt = new RotateTransition(Duration.millis(900), status);
            rt.setByAngle(-180);
            rt.setCycleCount(1);

            rt.play();
        }
        if (!data[8]){ //not praca
            status.setFill(Color.GRAY);
        }
        if (data[6]){ //awaria

        }
        if (data[5]){ //auto

        }
    }

    public void start(){
        if(!manager.Client.Connected) {
            return;
        }
        S7.SetBitAt(Buffer_in, 0, 0, true);
        manager.Client.WriteArea(S7.S7AreaDB, DBnum_in, addressInDB_in, 2, Buffer_in);
    }
    public void stop(){
        if(!manager.Client.Connected) {
            return;
        }
        S7.SetBitAt(Buffer_in, 0, 0, false);
        manager.Client.WriteArea(S7.S7AreaDB, DBnum_in, addressInDB_in, 2, Buffer_in);
    }
    public void auto_in() {
        if(!manager.Client.Connected) {
            return;
        }
        S7.SetBitAt(Buffer_in, 0, 1, true);
        manager.Client.WriteArea(S7.S7AreaDB, DBnum_in, addressInDB_in, 2, Buffer_in);
    }
    public void auto_in_off(){
        if(!manager.Client.Connected) {
            return;
        }
        S7.SetBitAt(Buffer_in, 0, 1, false);
        manager.Client.WriteArea(S7.S7AreaDB, DBnum_in, addressInDB_in, 2, Buffer_in);
    }
}
