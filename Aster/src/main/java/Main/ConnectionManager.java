package Main;

import com.sourceforge.snap7.moka7.*;

import javafx.scene.control.Alert;

public class ConnectionManager {

    public S7Client Client = new S7Client();
    public byte[] Buffer = new byte[1024];
    int connected = 1;

    public boolean connectToIP(String addresString){
        Client.SetConnectionType(S7.OP);
        if (addresString == null){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Błąd");
            alert.setHeaderText("Błąd adresu IP");
            alert.setContentText("Adres IP jest pusty!");
            alert.showAndWait();
            return false;
        }
        return Client.ConnectTo(addresString,0,0)==0;
    }

    // konwersja dwubajtowej liczby word do tablicy bitów
    public boolean[] toBoolArray(int b){
        boolean [] flags = new boolean[16];
        for (int i = 0; i < 16; ++i) {
            flags[i] = (b & (1 << i)) != 0;
        }
        return flags;
    }

}

