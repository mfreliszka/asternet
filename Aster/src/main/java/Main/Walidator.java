package Main;

import java.net.Inet4Address;
import java.net.UnknownHostException;

public class Walidator {

    public static boolean czyPoprawnyAdresIP(String ip)
    {
        try {
            return Inet4Address.getByName(ip)
                    .getHostAddress().equals(ip);
        }
        catch (UnknownHostException ex) {
            return false;
        }
    }

}
