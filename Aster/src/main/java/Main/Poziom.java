package Main;

import com.sourceforge.snap7.moka7.S7;
import javafx.application.Platform;
import javafx.scene.control.TextField;
import javafx.scene.shape.Rectangle;

//import javax.xml.bind.annotation.*;
//
//@XmlRootElement
//@XmlAccessorType(XmlAccessType.FIELD)
public class Poziom {
    public int poziom;
    public int addressInDB;
    public int DBnum;
    public String name;
    ConnectionManager manager;
    Rectangle rect;
    TextField txt;
    public static byte[] Buffer = new byte[2];

    public Poziom(String name, int DBnum, int addressInDB, ConnectionManager manager,TextField txt, Rectangle rect){
        this.name = name;
        this.DBnum = DBnum;
        this.addressInDB = addressInDB;
        this.manager = manager;
        this.txt = txt;
        this.rect = rect;
    }

    public void czytajDane() {
        if (!manager.Client.Connected) {
            return;
        }
        manager.Client.ReadArea(S7.S7AreaDB, DBnum, addressInDB, 2, Buffer);
        poziom = S7.GetWordAt(Buffer,0);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                txt.clear();
                txt.setText(String.valueOf(poziom) + " cm");
                rect.setHeight(poziom);
            }});

    }
}
