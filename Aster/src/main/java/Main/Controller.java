package Main;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.stage.Modality;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML private
    Rectangle status_transmisji;
    @FXML private
    MenuItem rozpocznij_transmisje, zakoncz_transmisje, ustawienia;
    @FXML private
    ConnectionManager manager;
    ReadDataThread czytDane;
    boolean czyPolaczony;

    public void rozpocznijTransmisje() {
        if(czyPolaczony==true)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Błąd");
            alert.setHeaderText("Juz połączono");
            alert.setContentText("Juz połączono");
            alert.showAndWait();
            return;
        }
        if(manager.connectToIP(Main.IP_ADDRESS)==false && !manager.Client.Connected)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Błąd");
            alert.setHeaderText("Błąd transmisji");
            alert.setContentText("Nie nawiązano transmisji!");
            alert.showAndWait();
            czyPolaczony=false;
            return;
        }
        try{
            czytDane = new ReadDataThread(manager);
            status_transmisji.setFill(javafx.scene.paint.Color.GREEN);
            System.out.println("Transmisja OK");
            czytDane.start();
            czyPolaczony=true;
        }
        catch(Exception ex){
            czytDane=null;
            czyPolaczony=false;
            throw ex;
        }

    }
    private void zakonczAktywnaTransmisje(){
        if(!czyPolaczony)
            return;
        try{        
            czytDane.stop();
            if(!manager.Client.Connected) {

                status_transmisji.setFill(javafx.scene.paint.Color.RED);
                System.out.println("Transmisja zatrzymana");
            }
        }
        catch(Exception ex)
        {
            System.out.println("Nie mozna zatrzymac watku");
        }
        finally{
            manager.Client.Disconnect();
            czytDane=null;
            czyPolaczony=false;
        }
    }
    public void zakonczTransmisje(){
        if(!czyPolaczony)
        {
            alertBrakTransmisji();
            return;
        }
        zakonczAktywnaTransmisje();
    }

    public void wyswietlUstawienia(){
        Stage window = new Stage();
        TextField ipInput = new TextField();
        ipInput.setPromptText("Adres IP sterownika");
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Konfiguracja");
        window.setMinWidth(350);
        window.setMinHeight(250);
        Label label1 = new Label();
        label1.setText("Podaj adres IP sterownika:");

        Button closeSettingsButton = new Button("Zapisz");
        closeSettingsButton.setOnAction(e -> {
            if(Walidator.czyPoprawnyAdresIP(ipInput.getText())) { // walidacja pola adresu IP
                Main.IP_ADDRESS = ipInput.getText();
                window.close();
                System.out.println(Main.IP_ADDRESS);
            }
            else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Błąd");
                alert.setHeaderText("Błąd adresu IP");
                alert.setContentText("Adres jest nieprawidłowy!");
                alert.showAndWait();
            }
        });
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label1, ipInput, closeSettingsButton);
        layout.setAlignment(Pos.CENTER);
        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();
    }

    @FXML
    private void zalaczSilnik(ActionEvent event){
        Node node = (Node) event.getSource() ;
        String data = (String) node.getUserData();
        int value = Integer.parseInt(data);
        System.out.println("zalaczSilnik "+value);
        if(!czyPolaczony)
        {
            alertBrakTransmisji();
            return;
        }
        
        
        Silnik silnik = Main.silniki.get(value);
        silnik.start();
    }
    @FXML
    private void wylaczSilnik(ActionEvent event){
        Node node = (Node) event.getSource() ;
        String data = (String) node.getUserData();
        int value = Integer.parseInt(data);
        System.out.println("wylaczSilnik "+value);
        if(!czyPolaczony)
        {
            alertBrakTransmisji();
            return;
        }
        Silnik silnik = Main.silniki.get(value);
        silnik.stop();
    }

    public void alertBrakTransmisji()
    {
        Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Błąd");
            alert.setHeaderText("Brak transmisji");
            alert.setContentText("Nie ma aktywnego połączenia");
            alert.showAndWait();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.manager = Main.manager;
        //czytDane = new Thread(new ReadDataThread(manager));;
    }

    public void shutdown() {
        System.out.println("wywolano shutdown");
        zakonczAktywnaTransmisje();
    }
    
}
