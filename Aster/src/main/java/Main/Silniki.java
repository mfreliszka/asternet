package Main;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "silniki")
@XmlAccessorType (XmlAccessType.FIELD)
public class Silniki
{

    @XmlElement(name = "silnik")
    private List<Silnik> silniki = new ArrayList<Silnik>();

    public Silniki(List<Silnik> silniki){
        this.silniki = silniki;
    }


    public List<Silnik> getSilniki() {
        return silniki;
    }

    public void setSilniki(List<Silnik> silniki) {
        this.silniki = silniki;
    }
}