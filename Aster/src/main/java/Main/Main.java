package Main;

import java.util.HashMap;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.shape.Rectangle;
import java.net.URL;
//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.JAXBException;
//import javax.xml.bind.Unmarshaller;
//import java.io.File;
//import java.util.List;

public class Main extends Application {

    public static String IP_ADDRESS = "192.168.1.50";
    public static FXMLLoader loader;
    public static ConnectionManager manager = new ConnectionManager();

    public static HashMap<Integer, Silnik> silniki;
    public static HashMap<Integer, Poziom> poziomy;
    public static Scene scene;

//    File xml = new File("obiekty.xml");

    @Override
    public void start(Stage stage) throws Exception {
        URL resource = getClass().getClassLoader().getResource("view.fxml");
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        }
        loader = new FXMLLoader(resource);//getClass().getResource("view.fxml"));
        Parent root = loader.load();
        Controller controller = loader.getController();
        stage.setTitle("Interfejs");
        stage.setScene(scene = new Scene(root, 600, 400));
        stage.setOnHidden(e -> controller.shutdown());
        stage.show();
//        try {
//            JAXBContext jaxbContext = JAXBContext.newInstance(Silniki.class);//, Poziom.class);
//            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//
//            silniki = (Silniki) jaxbUnmarshaller.unmarshal(xml);
//        } catch (JAXBException e) {
//            e.printStackTrace();
//        }
        silniki = new HashMap<>();
        silniki.put(1,new Silnik("P1", 1, 0, 2, 0, manager, (Rectangle) scene.lookup("#P1_status")));
        silniki.put(2,new Silnik("P2", 1, 2, 2, 2, manager, (Rectangle) scene.lookup("#P2_status")));
        silniki.put(3,new Silnik("P3", 1, 4, 2, 4, manager, (Rectangle) scene.lookup("#P3_status")));
        poziomy = new HashMap<>();
        poziomy.put(1,new Poziom("Poziom", 1, 6, manager, (TextField) scene.lookup("#poziom_cm"), (Rectangle) scene.lookup("#poziom_wody")));
    }

    public static void main(String[] args){
        Application.launch(args);
    }
}
