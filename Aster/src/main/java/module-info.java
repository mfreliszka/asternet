module Main {
    requires javafx.controls;
    requires javafx.fxml;
    requires moka7.live;
    requires java.xml;
    requires java.xml.bind;
    exports Main;

    opens Main to javafx.fxml;
}